import { ProgressBarPage } from './app.po';

describe('progress-bar App', () => {
  let page: ProgressBarPage;

  beforeEach(() => {
    page = new ProgressBarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
