import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressboxComponent } from './progressbox.component';

describe('ProgressboxComponent', () => {
  let component: ProgressboxComponent;
  let fixture: ComponentFixture<ProgressboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
