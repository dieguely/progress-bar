import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progressbox',
  templateUrl: './progressbox.component.html',
  styleUrls: ['./progressbox.component.scss']
})
export class ProgressboxComponent implements OnInit {

  public target = 125;
  public progress = 0;
  public boxWidth = 6;

  constructor(
  ) { }

  ngOnInit() {

    setTimeout(() => {
      this.progress = 56;
    },500);
  }

}
